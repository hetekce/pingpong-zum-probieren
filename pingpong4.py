"""
Ein Programm, das die Zahlen von 1 bis 100 ausgibt
Wenn eine Zahl durch 3 teilbar ist, gib stattdessen „Ping“ aus
Wenn eine Zahl durch 5 teilbar ist, gibt stattdessen „Pong“ aus
Wenn eine Zahl durch 3 und durch 5 teilbar ist, gibt „PingPong“ aus
"""

from pingpong3 import ausgabe


for i in range(1, 101):
    print(ausgabe(i))
