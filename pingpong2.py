"""
Ein Programm, das die Zahlen von 1 bis 100 ausgibt
Wenn eine Zahl durch 3 teilbar ist, gib stattdessen „Ping“ aus
Wenn eine Zahl durch 5 teilbar ist, gibt stattdessen „Pong“ aus
Wenn eine Zahl durch 3 und durch 5 teilbar ist, gibt „PingPong“ aus
"""

for k in range(100):
    if k % 3 == 0: 
        drei_teilbar = True
    if drei_teilbar:
        print("Ping")
    else:
        print(k)
